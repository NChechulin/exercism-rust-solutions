# Exercism Rust Solutions

This repo contains my solutions to problems from [Exercism](https://exercism.io/).

All of the solutions can be found in `rust` folder.
The tasks and info about running the programs are included.
